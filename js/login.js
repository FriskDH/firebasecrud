//FUNCION PARA MANDAR REGISTRARSE CON UN EMAIL Y CONTRASENIA
function signUp(){
    $('#btnRegistrarse').click(function () {
        var email = $('#txtEmail').val();
        var pass = $('#txtPass').val();
        if (email == '' || pass == '') {
            // console.log(' :v');
            $('#modalAdvertencia .modal-body').text('Llene los campos requeridos!');
            $('#modalAdvertencia').modal("show");
        } else {

            //IF PARA VERIFICAR LA VALIDEZ DEL FORMATO DE EMAIL INGRESADO
            if(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){

                firebase.auth().createUserWithEmailAndPassword(email, pass).catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if (errorCode || errorMessage) {

                        //DESPLEGAR MODAL CON EL MENSAJE DE ERROR
                        $('#modalAdvertencia .modal-body').text(errorMessage);
                        $('#modalAdvertencia').modal("show");
                    }
                    $('#txtEmail').val('');
                    $('#txtPass').val('');
                    // ...
                });

                //DESPLEGAR MODAL DE 'SUCCESS'
                $('#modalExito').modal("show");
                $('#txtEmail').val('');
                $('#txtPass').val('');

            }else {
                //DESPLEGAR MODAL PARA INDICAR ERROR EN EL FORMATO DE EMAIL INGRESADO
                $('#modalAdvertencia .modal-body').text("Error en el formato de e-mail ingresado");
                $('#modalAdvertencia').modal("show");
                $('#txtEmail').val('');
                $('#txtPass').val('');
            }


        }
    });

}


//FUNCION PARA VERIFICAR EL LOGIN
function logIn(){
    $('#btnIniciar').click(function () {
        var email = $('#txtEmail').val();
        var pass = $('#txtPass').val();
        if (email == '' || pass == '') {
            $('#modalAdvertencia .modal-body').text('Llene los campos requeridos!');
            $('#modalAdvertencia').modal("show");
        } else {
            //IF PARA VERIFICAR LA VALIDEZ DEL FORMATO DE EMAIL INGRESADO
            if(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
                firebase.auth().signInWithEmailAndPassword(email, pass).catch(function(error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    if (errorCode || errorMessage) {
                        //MOSTRAR EL MODAL DE ERROR CON EL MENSAJE
                        $('#modalAdvertencia .modal-body').text(errorMessage);
                        $('#modalAdvertencia').modal("show");
                    }
                    $('#txtEmail').val('');
                    $('#txtPass').val('');

                    // ...
                });
                //MOSTRAR MODAL DE 'SUCCESS' PARA INDICAR EL INICIO DE SESION
                // $('#modalExito .modal-body').text("Sesion iniciada");
                // $('#modalExito').modal("show");
                window.location= "inc/ingreso_productos.php";
                $('#txtEmail').val('');
                $('#txtPass').val('');
            }else {
                //DESPLEGAR MODAL PARA INDICAR ERROR EN EL FORMATO DE EMAIL INGRESADO
                $('#modalAdvertencia .modal-body').text("Error en el formato de e-mail ingresado");
                $('#modalAdvertencia').modal("show");
                $('#txtEmail').val('');
                $('#txtPass').val('');
            }


        }
    });

}

//FUNCION PARA ENVIAR UN EMAIL CON UN LINK PARA INGRESAR A UNA PAGINA DINAMICA
//***TODAVIA NO HE CREADO UN TEMPLATE PARA UNA PAGINA DINAMICA, SOLO REDIRIGE A UN LINK PARA UNA PAGINA DINAMICA
function emailLogIn(){
    $('#btnEnviarCorreo').click(function (){
        var email = $('#txtEmail').val();
        // var pass = $('#txtPass').val();
        if (email == '') {
            $('#modalAdvertencia .modal-body').text('Llene los campos requeridos!');
            $('#modalAdvertencia').modal("show");
        } else {
            //IF PARA VERIFICAR LA VALIDEZ DEL FORMATO DE EMAIL INGRESADO
            if(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)){
                var actionCodeSettings = {
                    /////////////////////////////////////////////////////////////////
                    //ESTE ES EL DYNAMIC LINK QUE SE TIENE QUE CREAR EN FIREBASE
                    url: 'https://appfirebase-204eb.firebaseapp.com/emilSignIn',
                    /////////////////////////////////////////////////////////////////
                    handleCodeInApp: true,
                    dynamicLinkDomain: 'appFireBaseKyu.page.link'
                };

                //CODIGO DE FIREBASE PARA ENVIAR EL LINK AL EMAIL DEL USUARIO
                firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)

                    .then(function() {
                        window.localStorage.setItem('emailForSignIn', email);
                        //DESPLEGAR MODAL DE EXITO Y LIMPIAR INPUTS
                        $('#modalExito .modal-body').text("e-Mail enviado exitosamente");
                        $('#modalExito').modal("show");
                        $('#txtEmail').val('');
                        $('#txtPass').val('');
                    })

                    .catch(function(error) {
                        //EN CASO DE ERROR DESPLEGAR EL MODAL DE ADVERTENCIA SEGUN SEA EL CASO
                        $('#modalAdvertencia .modal-body').text(error.code);
                        if (error.code=='auth/unauthorized-continue-uri'){
                            $('#modalAdvertencia .modal-body').text('Error, su e-mail no esta autorizado.');
                        }else{
                        $('#modalAdvertencia .modal-body').text('Se ha producido un error.');
                        }
                        $('#modalAdvertencia').modal("show");
                        //LIMPIAR INPUTS
                        $('#txtEmail').val('');
                        $('#txtPass').val('');
                    });


            }else {
                //SI EL FORMATO DE EMAIL NO ES VALIDO, DESPLEGAR EL MODAL DE ADVERTENCIA
                $('#modalAdvertencia .modal-body').text("Error en el formato de e-mail ingresado");
                $('#modalAdvertencia').modal("show");
                $('#txtEmail').val('');
                $('#txtPass').val('');
            }


        }

    });
}


$(document).ready(function() {
    // var firebaseConfig = {
    //     apiKey: "AIzaSyBstn7O9X0ACtHxGOKVOxEG9YaS7891gBs",
    //     authDomain: "appfirebase-204eb.firebaseapp.com",
    //     databaseURL: "https://appfirebase-204eb.firebaseio.com",
    //     projectId: "appfirebase-204eb",
    //     storageBucket: "appfirebase-204eb.appspot.com",
    //     messagingSenderId: "641016057218",
    //     appId: "1:641016057218:web:5e1e2f572d8dcec858d39b"
    // };
    // // Initialize Firebase
    // firebase.initializeApp(firebaseConfig);

    signUp();
    logIn();
    emailLogIn();
});