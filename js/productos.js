//FUNCION PARA AGREGAR PRODUCTOS
function agregarProducto() {
    //AL DAR CLICK EN 'AGREGAR'
    $('#btnGuardarProd').click(function () {
        const db= firebase.firestore();
        var cantidad = $('#txtCantidad').val();
        var codigo = $('#txtCodigo').val();
        var nombre= $('#txtNombre').val();
        var presentacion= $('#txtPresentacion').val();
        var precio= $('#txtPrecio').val();

        //VALIDACION PARA QUE NO SE INGRESEN DATOS VACIOS/NULOS
        if(cantidad=='' || codigo=='' || nombre=='' || presentacion=='' || precio==''){
            $('#modalAdvertencia .modal-body').text("Llene todos los campos");
            $('#modalAdvertencia').modal("show");
        }else {

            //AGREGAR LOS DATOS A UN DOCUMENTO
            db.collection("productos").add({
                nombre: nombre,
                precio: precio,
                cantidad: cantidad,
                presentacion: presentacion,
                codigo: codigo
            })
                .then(function (docRef) {
                    // console.log("Document written with ID: ", docRef.id);
                    //MOSTRAR MODAL EXITOSO, Y LIMPIAR INPUTS
                    $('#modalExito .modal-body').text("Producto Ingresado Exitosamente");
                    $('#modalExito').modal('show');
                    $('#txtCantidad').val('');
                    $('#txtCodigo').val('');
                    $('#txtNombre').val('');
                    $('#txtPrecio').val('');
                    $('#txtPresentacion').val('');
                })
                .catch(function (error) {
                    // console.error("Error adding document: ", error);
                    //SI HAY ERROR MOSTRAR EL ERROR EN UN MODAL ADVERTENCIA
                    $('#modalAdvertencia .modal-body').text(error);
                    $('#modalAdvertencia').modal("show");
                });
        }
    });

}

//FUNCION QUE RESPONDE AL BOTON 'DESPLEGAR' PARA MOSTRAR LOS PRODUCTOS ACTUALES
//Y TAMBIEN PERMITIR SU EDICION/ELIMINACION
function desplegarProductos(){
    const db= firebase.firestore();

    $('#btnDesplegarProductos').click(function() {

        //AL DAR CLICK SE AGREGAN LAS FILAS
        tablaProd= document.getElementById('tblBody');
        db.collection('productos').onSnapshot((querySnapshot) => {
            tablaProd.innerText= '';
            querySnapshot.forEach(doc => {
                //AGREGAR LAS FILAS A LA TABLA
                //1 FILA POR PRODUCTO
                tablaProd.innerHTML += `
                <tr>
                    <td>${doc.data().codigo}</td>
                    <td>${doc.data().nombre}</td>
                    <td>${doc.data().cantidad}</td>
                    <td>${doc.data().presentacion}</td>
                    <td>${doc.data().precio}</td>
                    <!--AGREGAR BOTON DE EDITAR Y ENVIAR PARMAETROS A LA FUNCION QUE DEPSLIEGA-->
                    <td><button class="btn btn-info" onclick="editar('${doc.data().codigo}',
                    '${doc.data().nombre}',
                    '${doc.data().cantidad}',
                    '${doc.data().presentacion}',
                    '${doc.data().precio}',
                    '${doc.id}')">Editar</button></td>
                    <!--AGREGAR BOTON DE ELIMINAR Y ENVIAR EL ID A LA FUNCION QUE DEPSLIEGA-->
                    <td><button class="btn btn-info" onclick="eliminar('${doc.id}')">Eliminar</button></td>
                </tr>
                `;
            })
        });
        });


}

//FUNCION PARA EDITAR EL PRODUCTO SELECCIONADO
function editar(codigo, nombre, cantidad, presentacion, precio, id){
    const db= firebase.firestore();
    //MOSTRAR EL BOTON PARA GUARDAR LOS CAMBIOS
    $('#btnGuardarCambios').prop('hidden', false);
    $('#txtCodigo').val(codigo);
    $('#txtNombre').val(nombre);
    $('#txtPrecio').val(precio);
    $('#txtPresentacion').val(presentacion);
    $('#txtCantidad').val(cantidad);

    //AL DAR CLICK EN GUARDAR CAMBIOS SE ALMACENARAN LOS VALORES QUE ESTEN EN LOS INPUT EN FIRESTORE
    $('#btnGuardarCambios').click(function() {
        //OBTENER NUEVOS VALORES
        var nombreUpdate = $('#txtNombre').val();
        var codigoUpdate = $('#txtCodigo').val();
        var cantidadUpdate = $('#txtCantidad').val();
        var precioUpdate = $('#txtPrecio').val();
        var presentacionUpdate = $('#txtPresentacion').val();

        //CODIGO DE FIREBASE PARA ACTUALIZAR EL DOCUMENTO(PRODUCTO)
        var prodToUpdate = db.collection("productos").doc(id);
        return prodToUpdate.update({
            nombre: nombreUpdate,
            precio: precioUpdate,
            cantidad: cantidadUpdate,
            presentacion: presentacionUpdate,
            codigo: codigoUpdate
        })
            //SE DESPLIEGA EL MODAL DE EXITO SI NO HAY PROBLEMAS Y SE ESCONDE EL BOTON DE GUARDAR CAMBIOS
            //Y SE LIMPIAN LOS INPUTS
            .then(function() {
                $('#modalExito .modal-body').text('Producto actualizado!');
                $('#modalExito').modal('show');
                $('#btnGuardarCambios').prop('hidden', true);
                $('#txtCantidad').val('');
                $('#txtCodigo').val('');
                $('#txtNombre').val('');
                $('#txtPrecio').val('');
                $('#txtPresentacion').val('');
            })
            .catch(function(error) {
                //EN CASO DE ERROR SE DESPLIEGA EL MODAL DE ERROR CON SU MENSAJE, Y SE LIMPIAN LOS INPUTS
                //SE ESCONDE EL BOTON DE GUARDAR CAMBIOS
                $('#modalAdvertencia .modal-body').text('Error al actualizar producto!'+error.toString());
                $('#modalAdvertencia').modal('show');
                $('#btnGuardarCambios').prop('hidden', true);
                $('#txtCantidad').val('');
                $('#txtCodigo').val('');
                $('#txtNombre').val('');
                $('#txtPrecio').val('');
                $('#txtPresentacion').val('');
            });
    });

}

//FUNCION PARA ELIMINAR PRODUCTO
function eliminar(id){
    const db = firebase.firestore();
    //CODIGO DE FIREBASE PARA ELIMINAR UN DOCUMENT
    db.collection("productos").doc(id).delete().then(function() {
        console.log("Document successfully deleted!");
        //MOSTRAR EL MODAL DE EXITO DE ELIMINACION
        $('#modalExito .modal-body').text('Producto eliminado!');
        $('#modalExito').modal('show');
    }).catch(function(error) {
        //MOSTRAR EL MODAL DE ERROR DE ELIMINACION
        console.error("Error removing document: ", error);
        $('#modalAdvertencia .modal-body').text('Error al eliminar producto!'+error.toString());
        $('#modalAdvertencia').modal('show');
    });
}

$(document).ready(function () {
    // PARAMETROS NECESARIOS PARA INICIARLIZAR LA APP DE FIREBASE
    var firebaseConfig = {
        apiKey: "AIzaSyBstn7O9X0ACtHxGOKVOxEG9YaS7891gBs",
        authDomain: "appfirebase-204eb.firebaseapp.com",
        databaseURL: "https://appfirebase-204eb.firebaseio.com",
        projectId: "appfirebase-204eb",
        storageBucket: "appfirebase-204eb.appspot.com",
        messagingSenderId: "641016057218",
        appId: "1:641016057218:web:5e1e2f572d8dcec858d39b"
    };
    //INICIALIZAR LA APP
    firebase.initializeApp(firebaseConfig);

    agregarProducto();
    desplegarProductos();
});