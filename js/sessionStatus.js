$(document).ready(function(){
    //PARAMETROS NECESARIOS PARA INICIARLIZAR LA APP DE FIREBASE
    var firebaseConfig = {
        apiKey: "AIzaSyBstn7O9X0ACtHxGOKVOxEG9YaS7891gBs",
        authDomain: "appfirebase-204eb.firebaseapp.com",
        databaseURL: "https://appfirebase-204eb.firebaseio.com",
        projectId: "appfirebase-204eb",
        storageBucket: "appfirebase-204eb.appspot.com",
        messagingSenderId: "641016057218",
        appId: "1:641016057218:web:5e1e2f572d8dcec858d39b"
    };
    //INICIALIZAR LA APP
    firebase.initializeApp(firebaseConfig);

    //FUNCION PARA VERIFICAR EL ESTADO DE LA SESION
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log(user);
            //SI ESTA AUTENTICADO EL USUARIO, SE REDIRIGE A ESTA DIRECCION
            window.location = "inc/ingreso_productos.php";
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
        }else{
            window.location = "index.php";
        }
    });

});