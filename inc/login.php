<?php

?>

<!--SCRIPT PARA LAS FUNCIONES DE LOGIN/REGISTRO/LOGIN_EMAIL-->
<script src="js/login.js" type="text/javascript"></script>
<!--CREACION DEL CONTAINER PARA LA 'CARD' DE LOGIN-->
<div class="container" id="panel">
    <div class="col-md-5 offset-3">
        <div class="card">
            <div class="card-header">
                Ingrese Credenciales
            </div>
            <div class="card-body" role="form">
<!--                INPUT DE EMAIL-->
                <div class="form-group">
                    <label for="txtEmail">e-Mail</label>
                    <input class="form-control" type="email" id="txtEmail" required>
                </div>
<!--                INPUT DE PASSWORD-->
                <div class="form-group">
                    <label for="txtPass">Password</label>
                    <input class="form-control" type="password" id="txtPass" required>
                </div>
                <br>
<!--                BOTONES PARA INICIAR SESION O REGISTRARSE-->
                <div class="form-group">
                    <button type="submit" id="btnIniciar" class="btn-info btn">Iniciar</button>
                    <button type="submit" id="btnRegistrarse" class="btn-info btn">Registrarse</button>
                </div>
<!--                BOTON PARA ENVIAR CORREO DE LOGIN POR MEDIO DE E-MAIL-->
                <div class="form-group">
                    <button type="submit" id="btnEnviarCorreo" class="btn-info btn col-md-12">Enviar e-Mail Verif.
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
