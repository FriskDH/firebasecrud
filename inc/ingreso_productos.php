<!DOCTYPE HTML>
<html lang="es">
<script src="../lib/js/jquery-3.4.1.min.js" type="text/javascript"></script>
<script src="../lib/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/productos.js" type="text/javascript"></script>
<!--ESTO ES EL HEADER COPIADO LITERALMENTE-->
<!--SOLO MODIFIQUE LAS PATHS DE LOS .CSS-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport">
    <link rel="stylesheet" href="../lib/css/bootstrap.css">
    <link rel="stylesheet" href="../lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/panel.css">
    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" href="../css/tables.css">
</head>
<body>

<!--MODAL ADVERTENCIA-->
<div class="modal" tabindex="-1" role="dialog" id="modalAdvertencia">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Atencion!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Se ha producido un error, intentelo de nuevo.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!--END MODAL-->

<!--MODAL EXITOSO-->
<div class="modal" tabindex="-1" role="dialog" id="modalExito">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Peticion Realizada</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Se ha enviado la peticion de registro exitosamente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!--END MODAL-->

<!--END DEL HEADER-->

<!--CREACION DEL CARD PARA EL INGRESO DE PRODUCTOS-->
<div class="container " id="panel">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                Ingrese/Busque Producto
            </div>
            <div class="card-body form-inline" role="form">
                <!--            INPUT CODIGO PRODUCTO-->
                <div class="form-group col-sm-3">
                    <label for="txtCodigo">Codigo</label>
                    <input class="form-control" type="text" id="txtCodigo" required>
                </div>
                <!--            INPUT NOMBRE PRODUCTO-->
                <div class="form-group col-sm-3">
                    <label for="txtNombre">Nombre</label>
                    <input class="form-control" type="text" id="txtNombre" required>
                </div>
                <!--            INPUT CANTIDAD PRODUCTO-->
                <div class="form-group col-sm-3">
                    <label for="txtCantidad">Cantidad</label>
                    <input class="form-control" type="number" id="txtCantidad" required>
                </div>
                <!--            INPUT PRESENTACION PRODUCTO-->
                <div class="form-group col-sm-3">
                    <label for="txtPresentacion">Presentacion</label>
                    <input class="form-control" type="text" id="txtPresentacion" required>
                </div>
                <!--            INPUT PRECIO PRODUCTO-->
                <div class="form-group col-sm-3">
                    <label for="txtPrecio">Precio</label>
                    <input class="form-control" type="text" id="txtPrecio" required>
                </div>

                <br>

            </div>
<!--            BOTONES PARA LAS ACCIONES RESPECTIVAS DE PRODUCTOS-->
            <div class="card-body form-group">
                <button type="submit" id="btnGuardarProd" class="btn-info btn">Guardar</button>
                <button type="submit" id="btnGuardarCambios" class="btn-info btn" hidden>Actualizar</button>
                <button type="submit" id="btnDesplegarProductos" class="btn-info btn">Desplegar</button>
            </div>
        </div>

    </div>


    <div class="col-md-12 offset-1">
        <br>
        <br>
        <br>
        <!--TABLA PARA POSTERIORMENTE DESPLEGAR LOS PRODUCTOS-->
        <table>
            <tr>
                <th>Codigo</th>
                <th>Nombre</th>
                <th>Cantidad</th>
                <th>Presentacion</th>
                <th>Precio</th>
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>
            <tbody id="tblBody">

            </tbody>
        </table>
        <!--END TABLE-->
    </div>
</div>

<!--FOOTER COPIADO LITERLAMENTE-->

</body>
<!--FOOTER PARA IMPLEMENTAR LOS JS RESPECTIVOS DE FIREBASE Y PARA QUE SE CARGUE EL .JS DEL LOGIN-->

<script src="https://www.gstatic.com/firebasejs/7.14.3/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.3/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.3/firebase-firestore.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
</html>

<!--END DEL FOOTER-->